package com.company;

import java.lang.reflect.Array;
import java.util.Random;

/**
 * Created by heinr on 09.10.2016.
 */
public class Main {

    public static void main(String[] args) throws Exception {

        int[] array = new int[25];

        Random rand = new Random();

        for (int i = 0; i < 25; i++) array[i] = rand.nextInt(29);

        sort(array);
    }

    public static void sort(int[] array) {

        int l = 0;
        int r = array.length - 1;

        quickSort(array, l, r);

        for (int i = 0; i < array.length; i++) {

            System.out.print(array[i]);
            System.out.print(" ");
        }
    }

    public static void quickSort(int[] array, int start, int end) {

        if (start >= end) {
            return;
        }

        int pivot = partition(array, start, end);
        quickSort(array, start, pivot - 1);
        quickSort(array, pivot + 1, end);

    }

    static int partitionStolen(int[] array, int start, int end) {
        int marker = start;
        for (int i = start; i <= end; i++) {
            if (array[i] <= array[end]) {
                int temp = array[marker]; // swap
                array[marker] = array[i];
                array[i] = temp;
                marker += 1;
            }
        }
        return marker - 1;
    }

    public static int partition(int[] array, int start, int end) {

        int mid = (end - start) / 2 + start;
        int axle = array[mid];
        int marker = 0;

        for (int i = start; i <= mid; i++) {
            if (array[i] >= axle) {
                for (int j = end; j >= mid; j--) {
                    if (array[j] <= axle) {
                        if (array[i] > array[j]) {
                            int hilfe = array[i];
                            array[i] = array[j];
                            array[j] = hilfe;
                        }
                        if (i == j) {
                            marker = i;
                        }
                        if (array[i] == axle) mid = i;
                        if (array[j] == axle) mid = j;
                    }
                }
            }
        }

        return marker;
    }

}
