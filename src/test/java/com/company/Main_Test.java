package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by heinr on 09.10.2016.
 */
public class Main_Test {

    @Test // Аннотация отмечает метод как тест
    public void partitionTest() {

        int array[] = {1, 2, 3, 7, 5, 4, 9, 6};
        int r = array.length - 1;
        int l = 0;

        Main checkPartition = new Main();
        assertEquals(6, checkPartition.partition(array, l, r));

    }

    @Test // Аннотация отмечает метод как тест
    public void sortTest() {

        int rightSortArray[] = {1, 2, 3, 4, 5, 6, 7, 9};

        int testArray[] = {1, 2, 3, 7, 5, 4, 9, 6};
        int r = testArray.length - 1;
        int l = 0;

        Main quckSort = new Main();
        quckSort.quickSort(testArray,l,r);

        assertArrayEquals(rightSortArray, testArray);

    }
}
